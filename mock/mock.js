'use strict'
const express = require('express')

const app = express()

app.get('/_cat/indices', (req, res) => {
  const response = `
  health status index  uuid                   pri rep docs.count docs.deleted store.size pri.store.size
  yellow open   foobar yqdv1MKjQGGUaiXOyAxrHg   1   1          0            0       283b           283b
  `
  res.status(200).send(response)
})

app.get('/foobar', (req, res) => {
  res.status(200).send('foobar')
})

app.get('/noindex', (req, res) => {
  res.status(404).send('foobaz')
})

app.put('/foobar', (req, res) => {
  const response = {"acknowledged":true,"shards_acknowledged":true,"index":"foobar"}
  res.status(200).json(response)
})

app.delete('/foobar', (req, res) => {
  const response = {"acknowledged":true}
  res.status(200).json(response)
})

app.get('/_aliases', (req, res) => {
  const response = {"foobar":{"aliases":{}}}
  res.status(200).json(response)
})

app.get('/_cluster/health', (req, res) => {
  const response = {"cluster_name":"foobar-cluster","status":"green","timed_out":false,"number_of_nodes":5,"number_of_data_nodes":5,"active_primary_shards":5,"active_shards":10,"relocating_shards":0,"initializing_shards":0,"unassigned_shards":0,"delayed_unassigned_shards":0,"number_of_pending_tasks":0,"number_of_in_flight_fetch":0,"task_max_waiting_in_queue_millis":0,"active_shards_percent_as_number":100.0}
  res.status(200).json(response)
})

app.post('/_aliases', (req, res) => {
  let body = ''
  let json = null
  req.on('data', (data) => {
    body += data
  })
  req.on('end', () => {
    json = JSON.parse(body)

    const response = {"acknowledged":true}
    res.status(200).json(response)
  })
})

app.post('/foobar/_bulk', (req, res) => {
  const response = {"took":79,"errors":false,"items":[{"index":{"_index":"foobar","_type":"_doc","_id":"OO","_version":1,"result":"created","_shards":{"total":2,"successful":1,"failed":0},"_seq_no":0,"_primary_term":1,"status":201}},{"index":{"_index":"foobar","_type":"_doc","_id":"ED","_version":1,"result":"created","_shards":{"total":2,"successful":1,"failed":0},"_seq_no":1,"_primary_term":1,"status":201}}]}
  res.status(200).json(response)
})

app.all('/*', (req, res) => {
  console.log(req.toString())
  res.status(200).json(req.params)
})


app.listen(7000, () => console.log('Ready.'))
