const { fullUrl, serverUrl, printResults, handleError, handleNoIndex, migrationFiles, program} = require('../lib/utils.js')
const fs = require('fs')

describe('common utility functions', () => {

  describe('serverUrl', () => {
    it('should build a proper url', () => {
      program.host = 'foohost'
      program.port = 123
      expect(serverUrl()).toEqual('http://foohost:123/')
    })
  })

  describe('fullUrl', () => {
    beforeEach(() => {
      program.host = 'foohost'
      program.port = 123
      program.index = null
      program.type = null
    })

    it('should provide the server url when no values are set', () => {
      expect(fullUrl()).toEqual('http://foohost:123/')
    })

    it('should add an index to the url', () => {
      program.index = 'someindex'
      expect(fullUrl()).toEqual('http://foohost:123/someindex/')
    })

    it('should append a type after the index if one is provided', () => {
      program.index = 'someindex'
      program.type = 'sometype'
      expect(fullUrl()).toEqual('http://foohost:123/someindex/sometype/')
    })

    it('should append a path to the end of the url', () => {
      expect(fullUrl('the_path')).toEqual('http://foohost:123/the_path')
    })

    it('should remove unwanted forward slashes from beginning of the path', () => {
      expect(fullUrl('/path')).toEqual('http://foohost:123/path')
    })
  })

  describe('printResults', () => {
    beforeEach(() => {
      program.json = null
      global.console = {
        log: jest.fn()
      }
    })

    it('should write a json message when json is specified', () => {
      program.json = true
      const msg = {"the": "message"}
      printResults(msg)
      expect(global.console.log).toHaveBeenCalledWith(JSON.stringify(msg))
    })

    it('should not print json when not specified', () => {
      program.json = false
      const msg = "{foobar: notjson}"
      printResults(msg)
      expect(global.console.log).toHaveBeenCalledWith(msg)
    })
  })

  describe('handleError', () => {
    beforeEach(() => {
      program.json = null
    })

    it('should return the json error when json is specified', () => {
      program.json = true
      const msg = {"error": "message"}
      results = handleError(msg)
      expect(results).toEqual(msg)
    })

    it('should throw an error when json not specified', () => {
      program.json = false
      const msg = 'some error message'
      expect(() => {
        handleError(msg)
      }).toThrowError(msg)
    })
  })

  describe('handleNoIndex', () => {
    beforeEach(() => {
      program.json = null
    })

    it('should write a json message to console if json specified', () => {
      program.json = true
      const msg = {'error': 'No index specified! Use --index <name>'}
      handleNoIndex()
      expect(handleNoIndex()).toEqual(msg)
    })

    it('should throw an error if no json specified', () => {
      expect.assertions(1)
      program.json = false
      const msg = 'No index specified! Use --index <name>'
      try {
        handleNoIndex()
      } catch (e) {
        expect(e.message).toEqual(msg)
      }
    })
  })

  describe('migrationFiles', () => {
    const originalReaddirSync = fs.readdirSync

    beforeEach(() => {
      const files = ['1.ldj', '10.ldj', '2.ldj', '20.ldj', '3.ldj']
      fs.readdirSync = jest.fn(originalReaddirSync)
      fs.readdirSync.mockReturnValue(files)
    })

    afterEach(() => {
      fs.readdirSync = originalReaddirSync
    });

    it('should return sorted list of files when level is null', () => {
      const results = migrationFiles('data', null)
      expect(results[0]).toEqual('1.ldj')
      expect(results[1]).toEqual('2.ldj')
      expect(results[2]).toEqual('3.ldj')
      expect(results[3]).toEqual('10.ldj')
      expect(results[4]).toEqual('20.ldj')
    })

    it('should return sorted list when level is default', () => {
      const results = migrationFiles('data', -1)
      expect(results[0]).toEqual('1.ldj')
      expect(results[1]).toEqual('2.ldj')
      expect(results[2]).toEqual('3.ldj')
      expect(results[3]).toEqual('10.ldj')
      expect(results[4]).toEqual('20.ldj')
    })

    it('should limit the files returnedd when level is supplied', () => {
      const results = migrationFiles('data', 3)
      expect(results.length).toEqual(3)
      expect(results[0]).toEqual('1.ldj')
      expect(results[1]).toEqual('2.ldj')
      expect(results[2]).toEqual('3.ldj')
    })

    it('should return proper list when level does not exist', () => {
      const results = migrationFiles('data', 14)
      expect(results.length).toEqual(4)
      expect(results[0]).toEqual('1.ldj')
      expect(results[1]).toEqual('2.ldj')
      expect(results[2]).toEqual('3.ldj')
      expect(results[3]).toEqual('10.ldj')
    })
  })
})

