'use strict'
const pkg = require('./package.json')
const { printResults, program } = require('./lib/utils')
const indexExists = require('./lib/index-exists.js')
const listIndices = require('./lib/list-indices.js')
const createIndex = require('./lib/create-index.js')
const removeIndex = require('./lib/remove-index.js')
const listAliases = require('./lib/list-aliases.js')
const createAlias = require('./lib/create-alias.js')
const removeAlias = require('./lib/remove-alias.js')
const moveAlias = require('./lib/move-alias.js')
const clusterHealth = require('./lib/cluster-health.js')
const bulkLoad = require('./lib/bulk-load.js')
const migrate = require('./lib/migrate.js')

program
  .version(pkg.version)
  .description(pkg.description)
  .usage('[options] <command> [...]')
  .option('-o, --host <hostname>', 'hostname [localhost]', 'localhost')
  .option('-p, --port <number>', 'port number [9200]', '9200')
  .option('-j, --json', 'format output as JSON')
  .option('-i, --index <name>', 'which index to use')
  .option('-l --level <number>', 'level to migrate to during migration', -1)
  .option('-t, --type <type>', 'default type for bulk operation or migrations')

program
  .command('index-exists <name>')
  .description('check if an index already exists in the cluster')
  .action((name) => {
    indexExists(name).then(printResults)
  })

program
  .command('list-indices')
  .description('get a list of indices in the cluster')
  .action(() => {
    listIndices().then(printResults)
  })

program
  .command('create-index <name>')
  .description('create an index')
  .action((name) => {
    createIndex(name).then(printResults)
  })

program
  .command('remove-index')
  .description('remove an index')
  .action(() => {
    removeIndex().then(printResults)
  })

program
  .command('list-aliases')
  .description('list aliases for all indices')
  .action(() => {
    listAliases()
      .then(printResults)
      .catch(printResults)
  })

program
  .command('create-alias <name>')
  .description('create an alias')
  .action((name) => {
    createAlias(name)
      .then(printResults)
      .catch(printResults)
  })

program
  .command('remove-alias <name>')
  .description('remove an alias')
  .action((name) => {
    removeAlias(name)
      .then(printResults)
      .catch(printResults)
  })

program
  .command('move-alias <name> <from-index> <to-index>')
  .description('move an alias from one index to another')
  .action((name, from, to) => {
    moveAlias(name, from, to)
      .then(printResults)
      .catch(printResults)
  })

program
    .command('cluster-health')
    .description('get the cluster health')
    .action(() => {
      clusterHealth()
        .then(printResults)
        .catch(printResults)
    })

program
  .command('bulk <file>')
  .description('read and perform bulk operations for the specified file')
  .action((file) => {
    bulkLoad(file)
      .then(printResults)
      .catch(printResults)
  })

program
  .command('migrate <directory>')
  .description('bulk load the files in directory to Elasticsearch. Uses the --level, --force, and --index options.')
  .action((dir) => {
    migrate(dir)
      .then(printResults)
      .catch(printResults)
  })


program.parse(process.argv)

if (!program.args.filter(arg => typeof arg == 'object').length) {
  program.help()
}
