Feature: Command-line tool to perform actions on Elasticsearch database

  Scenario: It should print the usage
    When I run the "esp" command
    Then I should see the output "Usage: esp [options] <command> [...]"

  Scenario: It should state if an index exists
    When I run the "esp index-exists foobar" command
    Then I should see the output "Index foobar exists"

  Scenario: It should state if an index does not exist
    When I run the "esp index-exists noindex" command
    Then I should see the output "Index noindex does not exist"

  Scenario: It should list the indices
    When I run the "esp list-indices" command
    Then I should see the output "open   foobar"

  Scenario: It should create a new index
    When I run the "esp create-index foobar" command
    Then I should see the output "\"shards_acknowledged\":true,\"index\":\"foobar\""

  Scenario: It should remove an existing index
    When I run the "esp remove-index -i foobar" command
    Then I should see the output "{\"acknowledged\":true}"

  Scenario: It should list the aliases
    When I run the "esp list-aliases" command
    Then I should see the output "{\"foobar\":{\"aliases\":{}}}"

  Scenario: It should create a new alias
    When I run the "esp create-alias fooalias -i foobar" command
    Then I should see the output "{\"acknowledged\":true}"

  Scenario: It should remove an alias
    When I run the "esp remove-alias fooalias -i foobar" command
    Then I should see the output "{\"acknowledged\":true}"

  Scenario: It should bulk update an index
    When I run the "esp bulk features/support/bulk_test.ldj -i foobar" command
    Then I should see the output "\"errors\":false"

  Scenario: It should get the cluster health
    When I run the "esp cluster-health" command
    Then I should see the output "cluster health: green"

  Scenario: It should move an alias from one index to another
    When I run the "esp move-index foobar foobar1 foobar2" command
    Then I should see the output "{\"acknowledged\":true}"
