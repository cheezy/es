
const { setWorldConstructor } = require("cucumber")

class ElasticsearchWorld {
  constructor() {
    this.process = null
    this.output = null
  }

}

setWorldConstructor(ElasticsearchWorld)
