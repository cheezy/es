const { When, Then } = require("cucumber")
const { expect } = require("chai")
const { spawn } = require('child_process')


When('I run the {string} command', (command, done) => {
  let args = command.split(' ')
  let cmd = args.shift()
  args.push('-p')
  args.push('7000')
  this.process = spawn('./bin/' + cmd, args)
  this.process.stdout.on('data', (data) => {
    this.output += data
  })
  this.process.on('exit', (code, signal) => {
    done()
  })

})

Then('I should see the output {string}', (expected) => {
  expect(this.output).to.include(expected)
})
