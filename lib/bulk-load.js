'use strict'
const rp = require('request-promise')
const fs = require('fs')
const { program, handleError, fullUrl} = require('./utils.js')

module.exports = async (file) => {
  program.json = true
  const options = {
    url: fullUrl('_bulk'),
    json: true,
    headers: {
      'content-type': 'application/json'
    }
  }
  try {
    let output = null
    let stream = fs.createReadStream(file)
    await stream.pipe(rp.post(options, async (err, res, body) => {
      output = body
    }))
    return output
  } catch(error) {
    console.log('error is ', error.toString())
    return handleError(error)
  }
}
