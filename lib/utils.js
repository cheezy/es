const program = require('commander')
const fs = require('fs')

const fullUrl = (path = '') => {
  let url = serverUrl()
  if (program.index) {
    url += program.index + '/'
    if (program.type) {
      url += program.type + '/'
    }
  }
  return url + path.replace(/^\/*/, '')
}

const serverUrl = () => {
  return `http://${program.host}:${program.port}/`
}

const printResults = (result) => {
  if (program.json) {
    console.log(JSON.stringify(result))
  }
  else {
    console.log(result)
  }
}

const handleError = (error) => {
  if (program.json) {
    return error
  }
  throw error
}

const handleNoIndex = () => {
  const msg = 'No index specified! Use --index <name>'
  if (!program.json) throw Error(msg)
  return {error: msg}
}

const migrationFiles = (dir, level) => {
  const files = fs.readdirSync(dir)
  const sorted = files.sort((a, b) => {return parseInt(a) - parseInt(b)})
  if (level === null || level === -1) return sorted
  return sorted.filter(item => parseInt(item) <= level)
}


module.exports = {
  fullUrl,
  serverUrl,
  printResults,
  handleError,
  handleNoIndex,
  migrationFiles,
  program
}
