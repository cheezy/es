'use strict'
const rp = require('request-promise')
const { fullUrl, handleError, program } = require('./utils.js')

module.exports = async (name) => {
  program.index = name
  try {
    return await rp.put(fullUrl())
  } catch(error) {
    return handleError(error)
  }
}
