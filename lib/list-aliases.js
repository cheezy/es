'use strict'
const rp = require('request-promise')
const { serverUrl, handleError } = require('./utils.js')

module.exports = async () => {
  try {
    return await rp(serverUrl() + '_aliases')
  } catch(error) {
    return handleError(error)
  }
}
