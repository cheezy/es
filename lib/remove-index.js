'use strict'
const rp = require('request-promise')
const { program, fullUrl, handleError, handleNoIndex } = require('./utils.js')

module.exports = async () => {
  if (!program.index) return handleNoIndex()

  try {
    return await rp.delete(fullUrl())
  } catch(error) {
    return handleError(error)
  }
}
