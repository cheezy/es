'use strict'
const rp = require('request-promise')
const { program, fullUrl } = require('./utils.js')

module.exports = async () => {
  const path = '_cluster/health'
  try {
    const body = await rp(fullUrl(path), { json: program.json })
    if(program.json) {
      return body
    }
    const response = JSON.parse(body)
    return 'cluster health: ' + response.status
  } catch(error) {
    return error
  }
}
