'use strict'
const rp = require('request-promise')
const { handleError, serverUrl } = require('./utils.js')

module.exports = async (name, from, to) => {
  const message =
    {actions:
      [
        {remove: {index: from, alias: name}},
        {add: {index: to, alias: name}}
      ]
    }
  const options = {
    url: serverUrl() + '_aliases',
    body: JSON.stringify(message),
    headers: {
      'content-type': 'application/json'
    }
  }
  try {
    return await rp.post(options)
  } catch(error) {
    console.log('got an error')
    return handleError(error)
  }
}
