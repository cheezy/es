'use strict'
const rp = require('request-promise')
const { fullUrl, handleError, program } = require('./utils.js')

module.exports = async () => {
  const path = program.json ? '_all' : '_cat/indices?v'
  try {
    return await rp({url: fullUrl(path), json: program.json})
  } catch(error) {
    return handleError(error)
  }
}

