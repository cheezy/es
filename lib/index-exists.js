'use strict'
const rp = require('request-promise')
const { fullUrl, program } = require('./utils.js')

const successMessage = (res, name) => {
  if (program.json) return {exists: true}
  return `Index ${name} exists`
}

const errorMessage = (res, name) => {
  if (program.json) return {exists: false}
  return `Index ${name} does not exist`
}

module.exports = async (name) => {
  program.index = name
  const options = {url: fullUrl(), json: program.json, resolveWithFullResponse: true}
  try {
    const res = await rp.head(options)
    return successMessage(res, name)
  } catch (error) {
    if (error.statusCode === 404) return errorMessage(error, name)
    throw error
  }
}
