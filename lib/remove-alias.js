'use strict'
const rp = require('request-promise')
const { program, handleNoIndex, serverUrl, handleError } = require('./utils.js')

module.exports = async (name) => {
  if (!program.index) return handleNoIndex()

  const options = {
    url: serverUrl() + '_aliases',
    body: JSON.stringify({actions: [{remove: {index: program.index, alias: name}}]}),
    headers: {
      'content-type': 'application/json'
    }
  }
  try {
    return await rp.post(options)
  } catch(error) {
    return handleError(error)
  }
}
