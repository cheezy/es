'use strict'
const {migrationFiles, handleNoIndex, program} = require('./utils.js')
const indexExists = require('./index-exists.js')
const createIndex = require('./create-index.js')
const bulkLoad = require('./bulk-load.js')
const listAliases = require('./list-aliases.js')
const createAlias = require('./create-alias.js')
const moveAlias = require('./move-alias.js')

const writeTextMessage = (message) => {
  program.json = false
  return message
}

const createMyIndex = async () => {
  let indexCreated = null
  await createIndex(program.index).then((body) => {
    indexCreated = JSON.parse(body)
  })
  return indexCreated.acknowledged
}

const migrateThisFile = async (filename) => {
  console.log(`processing file ${filename}`)
  const result = await bulkLoad(filename)
  return !result.errors
}

const migrateAllFiles = async (dir, files) => {
  for (let i = 0; i < files.length; i++) {
    await migrateThisFile(`${dir}/${files[i]}`)
  }
}

const findIndexForAlias = async (alias) => {
  let aliases = await listAliases()
  aliases = await JSON.parse(aliases)
  const keys = Object.keys(aliases)
  for (let i = 0; i < keys.length; i++) {
    const element = aliases[keys[i]]
    if (element.aliases[alias]) {
      return keys[i]
    }
  }
  return null
}

const createOrMoveAlias = async (alias, fromIndex) => {
  if (fromIndex == null) {
    return await createAlias(alias)
  } else {
    return await moveAlias(alias, fromIndex, program.index)
  }
}

module.exports = async (dir) => {
  program.json = true
  if (!program.index) return handleNoIndex()

  if (program.level === -1) console.log('Option --level not provided: migrating all data files')

  const originalType = program.type
  program.type = null

  const files = migrationFiles(dir, program.level)
  const alias = program.index
  program.index = `${program.index}${parseInt(files[files.length - 1])}`

  let indexToLocate = null
  await indexExists(program.index).then((body) => {
    indexToLocate = body
  })

  if (indexToLocate.exists) {
    const message =
      `Index ${program.index} already exists.  Not updating Elasticsearch!`
    return writeTextMessage(message)
  }

  const created = await createMyIndex()
  if (!created) return writeTextMessage(`Unable to create index ${program.index}`)

  program.type = originalType
  await migrateAllFiles(dir, files)
  program.type = null

  const fromIndex = await findIndexForAlias(alias)
  // check return value
  await createOrMoveAlias(alias, fromIndex)


  const finalMessage = `index ${program.index} created and alias ${alias} is now pointing to it.`
  return writeTextMessage(finalMessage)
}
